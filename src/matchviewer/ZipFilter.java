/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matchviewer;

import java.io.File;
import java.io.FilenameFilter;

/**
 *
 * @author MacMini
 */
public class ZipFilter  implements FilenameFilter {
    
    @Override
    public boolean accept(File dir, String name)
    {
        // Show only files ending with ".zip"
        int dot = name.lastIndexOf('.');
        if (dot > 0)
        {
            if (name.substring(dot).equalsIgnoreCase(".zip"))
                return true;
            else
                return false;
        }
        else return false;
    } 
}
