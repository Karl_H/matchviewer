/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matchviewer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.JPanel;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author MacMini
 */
public class CrosswordPanel extends JPanel {

    public static final int BOX_SIZE = 45;
    public static final int NUM_VERTICAL = 12;
    public static final int NUM_HORIZONTAL = 9;

    JSONObject obj;

    // Various image objects
    public Image crosswordImage;
    public Image pix1, pix2;
    public static final int BIG_ARROWS_TO_LOAD[] = {
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 18, 23, 25, 30, 31, 33, 38
    };
    public Image bigArrows[] = new Image[40];
    //FIXME : add arrows to res
    public Image smallArrows[] = new Image[29];
    public Image greenBlob, redBlob, grayBlob;

    int blobOffset;

    int picX, picW, picY, picH;
    int boxType[][] = new int[NUM_VERTICAL][NUM_HORIZONTAL];
    // Word indicator lines
    boolean turnRight[][] = new boolean[NUM_VERTICAL][NUM_HORIZONTAL];
    boolean turnDown[][] = new boolean[NUM_VERTICAL][NUM_HORIZONTAL];
    // Word connectors
    boolean connectRight[][] = new boolean[NUM_VERTICAL][NUM_HORIZONTAL];
    boolean connectDown[][] = new boolean[NUM_VERTICAL][NUM_HORIZONTAL];

    // Clue box sizes and locations
    int clueX[] = new int[100];
    int clueY[] = new int[100];
    int clueW[] = new int[100];
    int clueH[] = new int[100];
    int numKeys;
    String keyTexts[][] = new String[100][];
    int clueFontSize[] = new int[100];

    // Positions and types for arrows in crossword
    int bigArrowX[] = new int[20];
    int bigArrowY[] = new int[20];
    int bigArrowType[] = new int[20];
    int numBigArrows;
    int smallArrowX[] = new int[30];
    int smallArrowY[] = new int[30];
    int smallArrowType[] = new int[30];
    int numSmallArrows;

    boolean multiSol;

    // Positions of each letter in each solution word
    int wordH[][] = new int[100][NUM_VERTICAL + NUM_HORIZONTAL];
    int wordV[][] = new int[100][NUM_VERTICAL + NUM_HORIZONTAL];
    int wordLen[] = new int[100];
    int numWords;

    public static final Color playerGreen = new Color(0, 120, 100);
    public static final Color playerRed = new Color(235, 70, 55);

    // Small arrow offsets in boxes
    public static final int SMALL_ARROW_DX[] = {
            1, 1, 35, 3, 1, 0, 1, 35, 3, 1, 3, 3, 0, 22, 22, 0, 18, 1, -4, 22, -2, 1, 1, 35, 33
    };
    public static final int SMALL_ARROW_DY[] = {
            3, 3, 3, 0, 1, 35, 1, 1, 33, 3, 1, 35, 22, 0, 0, 22, 1, 18, 22, -4, -2, 33, 33, 2, 1
    };

    public boolean visibleClues[] = new boolean[40];
    public char solution[][] = new char[NUM_VERTICAL][NUM_HORIZONTAL];
    public int colors[][] = new int[NUM_VERTICAL][NUM_HORIZONTAL];
    public int deblur;

    public BufferedImage backBuffer;
    public Graphics2D bg;

    // Font management
    public Font solutionFont;
    public FontMetrics solutionMetrics;
    public static final int SOLUTION_FONT_SIZE = 30;
    public static final int FONT_SIZES[] = {11, 10, 9, 8, 7, 6, 5, 4, 3};
    Font clueFonts[] = new Font[FONT_SIZES.length];
    FontMetrics clueMetrics[] = new FontMetrics[FONT_SIZES.length];

    boolean hasData = false;
    String logLines[] = null;
    int currentLogLine = 0;

    // Identifiers for log text lines that contain the match state
    final static String STRING1 = "\"type\":\"UPDATE_MATCH_DATA\"";
    final static String STRING2 = "\"type\":\"TURN_FINISHED\"";
    final static String STRING3 = "\"type\":\"MOVE_RESPONSE\"";
    final static String STRING4 = "\"type\":\"MATCH_INFO\"";

    public int score0, score1;

    public void initSize() {

        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(BOX_SIZE * NUM_HORIZONTAL + 2, BOX_SIZE * NUM_VERTICAL + 2));
        loadImagePieces();
        prepareFonts();
    }

    @Override
    public void paint(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, BOX_SIZE * NUM_HORIZONTAL + 1, BOX_SIZE * NUM_VERTICAL + 1);
        if (hasData) {
            g.drawImage(backBuffer, 1, 1, this);
        }
    }

    // Get crossword metadata from zip file
    public void loadCrossword(String cf) {
        try {
            ZipInputStream zis
                    = new ZipInputStream(new FileInputStream(cf));
            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {
                String fileName = ze.getName();
                if (fileName.indexOf(".jpg") > 0) {
                    // The image file
                    crosswordImage = ImageIO.read(zis);
                } else if (fileName.indexOf(".txt") > 0) {
                    // The JSON file
                    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

                    int nRead;
                    byte[] data = new byte[16384];

                    while ((nRead = zis.read(data, 0, data.length)) != -1) {
                        buffer.write(data, 0, nRead);
                    }
                    buffer.flush();
                    String js = new String(buffer.toByteArray());
                    multiSol = (js.indexOf("solutions") > 0);
                    obj = new JSONObject(js);

                } else if (fileName.equalsIgnoreCase("pix1.png")) {
                    pix1 = ImageIO.read(zis);
                } else if (fileName.equalsIgnoreCase("pix2.png")) {
                    pix2 = ImageIO.read(zis);
                }

                ze = zis.getNextEntry();
            }

            processJSON();
            showEverything();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Parse the crossword structure information
    public void processJSON() {
        JSONArray imgA = obj.getJSONArray("images");
        JSONObject objImg1 = imgA.getJSONObject(0);
        int imW = objImg1.getInt("width");
        int imH = objImg1.getInt("height");
        int picHorizontal = objImg1.getInt("posX");
        picX = BOX_SIZE * picHorizontal;
        int picVertical = objImg1.getInt("posY");
        picY = BOX_SIZE * picVertical;
        picW = BOX_SIZE * imW;
        picH = BOX_SIZE * imH;
        crosswordImage = crosswordImage.getScaledInstance(picW, picH, Image.SCALE_AREA_AVERAGING);
        pix1 = pix1.getScaledInstance(picW, picH, Image.SCALE_SMOOTH);
        pix2 = pix2.getScaledInstance(picW, picH, Image.SCALE_SMOOTH);

        JSONArray boxA = obj.getJSONArray("grid");
        String boxStrings[] = new String[NUM_VERTICAL];
        for (int i = 0; i < NUM_VERTICAL; i++) {
            boxStrings[i] = boxA.getString(i);
            for (int j = 0; j < NUM_HORIZONTAL; j++) {
                switch (boxStrings[i].charAt(j)) {
                    case '0':
                        boxType[i][j] = 0;
                        break;
                    case '1':
                        boxType[i][j] = 1;
                        break;
                    default:
                        boxType[i][j] = 8;
                        break;
                }
            }
        }

        // Parse small arrows and make note of word turn indicators
        JSONArray smaA = obj.getJSONArray("arrows");
        for (int i = 0; i < smaA.length(); i++) {
            JSONObject arrO = smaA.getJSONObject(i);
            int h = arrO.getInt("x") / 40;
            int v = arrO.getInt("y") / 40;
            int t = arrO.getInt("type");
            if (t == 8) {
                turnRight[v][h] = true;
            } else if (t == 9) {
                turnDown[v][h] = true;
            } else if (t == 17) {
                connectRight[v][h] = true;
            } else if (t == 16) {
                connectDown[v][h] = true;
            }
        }

        numWords = 0;
        JSONArray keyA = obj.getJSONArray("keys");
        numKeys = keyA.length();
        String textLines[] = new String[20];
        int numLines;
        // Parse clue boxes
        for (int i = 0; i < numKeys; i++) {
            JSONObject keyObj = keyA.getJSONObject(i);
            int kX = keyObj.getInt("x");
            int kY = keyObj.getInt("y");
            int kW = keyObj.getInt("width");
            int kH = keyObj.getInt("height");

            // Convert width and height data into actual sizes
            int kDX = kX % 40;
            int kDY = kY % 40;
            int keyX = 0, keyY = 0, keyW = 0, keyH = 0;
            int keyDX = 0, keyDY = 0;
            if (kW == 41) {
                keyW = 45;
            } else if (kW == 21) {
                keyW = 22;
            } else if (kW == 81) {
                keyW = 90;
            } else if (kW == 121) {
                keyW = 135;
            } else if (kW == 28 || kW == 27) {
                keyW = 30;
            } else if (kW == 54) {
                keyW = 60;
            } else if (kW == 14 || kW == 15) {
                keyW = 15;
            }
            if (kH == 14 || kH == 15) {
                keyH = 15;
            } else if (kH == 21) {
                keyH = 22;
            } else if (kH == 28 || kH == 27) {
                keyH = 30;
            } else if (kH == 41) {
                keyH = 45;
            } else if (kH == 54) {
                keyH = 60;
            } else if (kH == 81) {
                keyH = 90;
            } else if (kH == 121) {
                keyH = 135;
            }
            if (kDX == 14 || kDX == 13) {
                keyDX = 15;
            } else if (kDX == 21 || kDX == 20) {
                keyDX = 22;
            } else if (kDX == 28 || kDX == 27) {
                keyDX = 30;
            }
            if (kDY == 14 || kDY == 13) {
                keyDY = 15;
            } else if (kDY == 21 || kDY == 20) {
                keyDY = 22;
            } else if (kDY == 28 || kDY == 27) {
                keyDY = 30;
            }
            keyX = BOX_SIZE * (kX / 40) + keyDX;
            keyY = BOX_SIZE * (kY / 40) + keyDY;
            if ((keyX + keyW) % BOX_SIZE == BOX_SIZE - 1) {
                keyW++;
            }
            if ((keyY + keyH) % BOX_SIZE == BOX_SIZE - 1) {
                keyH++;
            }
            clueX[i] = keyX;
            clueY[i] = keyY;
            clueW[i] = keyW;
            clueH[i] = keyH;

            String keyText = keyObj.getString("text");
            numLines = 0;
            StringTokenizer st = new StringTokenizer(keyText, "\n");
            while (st.hasMoreElements()) {
                textLines[numLines++] = st.nextToken();
            }
            keyTexts[i] = new String[numLines];
            System.arraycopy(textLines, 0, keyTexts[i], 0, numLines);

            // Find a small enough font to fit the clue text into the box
            boolean fits = false;
            int useFont = 0;
            while (!fits && useFont < FONT_SIZES.length) {
                fits = true;
                if (clueMetrics[useFont].getAscent() * (numLines + 1) > clueW[i]) {
                    fits = false;
                }
                for (int j = 0; j < numLines; j++) {
                    if (8 * clueMetrics[useFont].stringWidth(textLines[j]) / 7 >= clueW[i]) {
                        fits = false;
                    }
                }
                if (!fits) {
                    useFont++;
                }
            }
            clueFontSize[i] = useFont;

            // Find each solution word
            JSONArray tArr = keyObj.getJSONArray("targets");
            for (int j = 0; j < tArr.length(); j++) {
                JSONObject tObj = tArr.getJSONObject(j);
                int h = tObj.getInt("h");
                int v = tObj.getInt("v");
                boolean horizontal = tObj.getBoolean("horizontal");
                boolean keepGoing = true;
                int wLen = 0;
                while (keepGoing) {
                    wordH[numWords][wLen] = h;
                    wordV[numWords][wLen] = v;
                    wLen++;
                    keepGoing = false;
                    if (horizontal) {
                        if (h < NUM_HORIZONTAL - 1 && boxType[v][h + 1] < 2) {
                            h++;
                            keepGoing = true;
                        } else if (turnDown[v][h] && v < NUM_VERTICAL - 1) {
                            v++;
                            horizontal = false;
                            keepGoing = true;
                        }
                    } else {
                        if (v < NUM_VERTICAL - 1 && boxType[v + 1][h] < 2) {
                            v++;
                            keepGoing = true;
                        } else if (turnRight[v][h] && h < NUM_HORIZONTAL - 1) {
                            h++;
                            horizontal = true;
                            keepGoing = true;
                        }
                    }
                }
                wordLen[numWords] = wLen;
                numWords++;
            }
        }
        // Look for yellow words
        if (!obj.has("yellow")) {
            for (int i = 0; i < NUM_VERTICAL; i++) {
                for (int j = 0; j < NUM_HORIZONTAL; j++) {
                    // Check for start of yellow word
                    if (boxType[i][j] == 1) {
                        boolean connectUp = (i > 0 && boxType[i - 1][j] == 1);
                        boolean connectLeft = (j > 0 && boxType[i][j - 1] == 1);
                        boolean connectDown = (i < NUM_VERTICAL - 1 && boxType[i + 1][j] == 1);
                        boolean connectRight = (j < NUM_HORIZONTAL - 1 && boxType[i][j + 1] == 1);

                        boolean startRight = false;
                        boolean startDown = false;
                        if (!connectUp && !connectLeft) // Origin?
                        {
                            startDown = connectDown;
                            startRight = connectRight;
                        } else if (!connectUp && connectLeft && connectRight) // T-junction down?
                        {
                            startDown = connectDown;
                        } else if (!connectLeft && connectUp && connectDown) // T-junction right?
                        {
                            startRight = connectRight;
                        }
                        boolean horizontal;
                        boolean keepGoing;
                        int v, h, p;
                        if (startRight) // New rightward yellow word
                        {
                            v = i;
                            h = j;
                            wordH[numWords][0] = h;
                            wordV[numWords][0] = v;
                            p = 1;
                            horizontal = true;
                            keepGoing = true;
                            while (keepGoing) {
                                keepGoing = false;
                                if (horizontal) {
                                    h++;
                                    if (h < NUM_HORIZONTAL - 1 && boxType[v][h + 1] == 1) {
                                        keepGoing = true;
                                    } else if (h < NUM_HORIZONTAL && v < NUM_VERTICAL - 1 && boxType[v + 1][h] == 1 && !(v > 0 && boxType[v - 1][h] == 1)) {
                                        horizontal = false;
                                        keepGoing = true;
                                    }
                                } else {
                                    v++;
                                    if (v < NUM_VERTICAL - 1 && boxType[v + 1][h] == 1) {
                                        keepGoing = true;
                                    } else if (v < NUM_VERTICAL && h < NUM_HORIZONTAL - 1 && boxType[v][h + 1] == 1 && !(h > 0 && boxType[v][h - 1] == 1)) {
                                        horizontal = true;
                                        keepGoing = true;
                                    }
                                }
                                wordH[numWords][p] = h;
                                wordV[numWords][p] = v;
                                p++;
                            }
                            wordLen[numWords] = p;
                            numWords++;
                        }
                        if (startDown) // New downward yellow word
                        {
                            v = i;
                            h = j;
                            wordH[numWords][0] = h;
                            wordV[numWords][0] = v;
                            p = 1;
                            horizontal = false;
                            keepGoing = true;
                            while (keepGoing) {
                                keepGoing = false;
                                if (horizontal) {
                                    h++;
                                    if (h < NUM_HORIZONTAL - 1 && boxType[v][h + 1] == 1) {
                                        keepGoing = true;
                                    } else if (h < NUM_HORIZONTAL && v < NUM_VERTICAL - 1 && boxType[v + 1][h] == 1 && !(v > 0 && boxType[v - 1][h] == 1)) {
                                        horizontal = false;
                                        keepGoing = true;
                                    }
                                } else {
                                    v++;
                                    if (v < NUM_VERTICAL - 1 && boxType[v + 1][h] == 1) {
                                        keepGoing = true;
                                    } else if (v < NUM_VERTICAL && h < NUM_HORIZONTAL - 1 && boxType[v][h + 1] == 1 && !(h > 0 && boxType[v][h - 1] == 1)) {
                                        horizontal = true;
                                        keepGoing = true;
                                    }
                                }
                                wordH[numWords][p] = h;
                                wordV[numWords][p] = v;
                                p++;
                            }
                            wordLen[numWords] = p;
                            numWords++;
                        }
                        if (!connectDown && !connectUp && !connectLeft && !connectRight) // Single letter
                        {
                            wordH[numWords][0] = j;
                            wordV[numWords][0] = i;
                            wordLen[numWords] = 1;
                            numWords++;
                        }
                    }
                }
            }
        }

        // Big arrow data
        JSONArray bgA = obj.getJSONArray("bigArrows");
        numBigArrows = bgA.length();
        for (int i = 0; i < numBigArrows; i++) {
            JSONObject arrO = bgA.getJSONObject(i);
            bigArrowX[i] = BOX_SIZE * (arrO.getInt("x") / 40);
            bigArrowY[i] = BOX_SIZE * (arrO.getInt("y") / 40);
            bigArrowType[i] = arrO.getInt("type");
        }

        // Small arrow data
        JSONArray smA = obj.getJSONArray("arrows");
        numSmallArrows = smA.length();
        for (int i = 0; i < numSmallArrows; i++) {
            JSONObject arrO = smA.getJSONObject(i);
            smallArrowType[i] = arrO.getInt("type");
            smallArrowX[i] = BOX_SIZE * (arrO.getInt("x") / 40) + (smallArrowType[i] > SMALL_ARROW_DX.length ? 0 : SMALL_ARROW_DX[smallArrowType[i]]);
            smallArrowY[i] = BOX_SIZE * (arrO.getInt("y") / 40) + (smallArrowType[i] > SMALL_ARROW_DY.length ? 0 : SMALL_ARROW_DY[smallArrowType[i]]);
        }

        String solS;
        if (multiSol) {
            JSONArray solA = obj.getJSONArray("solutions");
            solS = solA.getString(0);
        } else {
            solS = obj.getString("solution");
        }
        for (int i = 0; i < NUM_VERTICAL; i++) {
            for (int j = 0; j < NUM_HORIZONTAL; j++) {
                solution[i][j] = solS.charAt(i * NUM_HORIZONTAL + j);
            }
        }

        hasData = true;
    }

    public void loadLog(String lf) {
        try {
            FileReader fileReader = new FileReader(lf);

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            List<String> lines = new ArrayList<String>();
            String line = null;

            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
            bufferedReader.close();

            logLines = lines.toArray(new String[lines.size()]);
            currentLogLine = 0;

            clearBoard();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void prepareBuffers() {
        backBuffer = new BufferedImage(BOX_SIZE * NUM_HORIZONTAL, BOX_SIZE * NUM_VERTICAL, BufferedImage.TYPE_INT_RGB);
        bg = (Graphics2D) backBuffer.getGraphics();
        bg.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
    }

    public void prepareFonts() {
        // Load Tekton Pro Bold from resources
        try {
            InputStream is = this.getClass().getResourceAsStream("/res/an_demi_bold.ttf");
            Font tektonBase = Font.createFont(Font.TRUETYPE_FONT, is);
            //solutionFont = new Font("Tekton Pro", Font.PLAIN, SOLUTION_FONT_SIZE);
            solutionFont = tektonBase.deriveFont(Font.BOLD, SOLUTION_FONT_SIZE);
            solutionMetrics = this.getFontMetrics(solutionFont);
            for (int i = 0; i < FONT_SIZES.length; i++) {
                //clueFonts[i] = new Font("Tekton Pro", Font.PLAIN, FONT_SIZES[i]);
                clueFonts[i] = tektonBase.deriveFont(Font.BOLD, FONT_SIZES[i]);
                clueMetrics[i] = this.getFontMetrics(clueFonts[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadImagePieces() {
        try {
            for (int i = 0; i < BIG_ARROWS_TO_LOAD.length; i++) {
                int j = BIG_ARROWS_TO_LOAD[i];
                bigArrows[j] = ImageIO.read(this.getClass().getResourceAsStream("/res/bigarrow" + j + ".png"));
            }
            for (int i = 0; i < smallArrows.length; i++) {
                try {
                    smallArrows[i] = ImageIO.read(this.getClass().getResourceAsStream("/res/arrow-" + i + ".png"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            greenBlob = ImageIO.read(this.getClass().getResourceAsStream("/res/greenblob.png"));
            redBlob = ImageIO.read(this.getClass().getResourceAsStream("/res/redblob.png"));
            grayBlob = ImageIO.read(this.getClass().getResourceAsStream("/res/grayblob.png"));
            blobOffset = (BOX_SIZE - greenBlob.getWidth(this)) / 2;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Render the match state
    public void drawCrossword() {

        score0 = 0;
        score1 = 0;

        if (backBuffer == null) {
            prepareBuffers();
        }
        bg.setColor(Color.WHITE);
        bg.fillRect(0, 0, BOX_SIZE * NUM_HORIZONTAL, BOX_SIZE * NUM_VERTICAL);

        if (deblur == 2) {
            bg.drawImage(crosswordImage, picX, picY, this);
        } else if (deblur == 1) {
            bg.drawImage(pix1, picX, picY, this);
        } else {
            bg.drawImage(pix2, picX, picY, this);
        }

        bg.setColor(Color.BLACK);
        bg.drawRect(0, 0, BOX_SIZE * NUM_HORIZONTAL - 1, BOX_SIZE * NUM_VERTICAL - 1);
        bg.drawRect(picX, picY, picW - 1, picH - 1);

        for (int i = 0; i < NUM_VERTICAL; i++) {
            for (int j = 0; j < NUM_HORIZONTAL; j++) {
                if (boxType[i][j] == 0 || boxType[i][j] == 1) {
                    bg.drawRect(BOX_SIZE * j, BOX_SIZE * i, BOX_SIZE - 1, BOX_SIZE - 1);
                    if (boxType[i][j] == 1) {
                        bg.setColor(Color.YELLOW);
                        bg.fillRect(BOX_SIZE * j + 1, BOX_SIZE * i + 1, BOX_SIZE - 2, BOX_SIZE - 2);
                        bg.setColor(Color.BLACK);
                    }
                }
            }
        }

        for (int i = 0; i < numKeys; i++) {
            bg.drawRect(clueX[i], clueY[i], clueW[i] - 1, clueH[i] - 1);
        }

        for (int i = 0; i < numBigArrows; i++) {
            bg.drawImage(bigArrows[bigArrowType[i]], bigArrowX[i], bigArrowY[i], this);
        }
        for (int i = 0; i < numSmallArrows; i++) {
            bg.drawImage(smallArrows[smallArrowType[i]], smallArrowX[i], smallArrowY[i], this);
        }

        // Filled-in words
        for (int i = 0; i < numWords; i++) {
            int h = wordH[i][0];
            int v = wordV[i][0];
            int sameColor = colors[v][h];
            if (wordLen[i] < 2) {
                sameColor = 0;
            }
            for (int j = 1; j < wordLen[i]; j++) {
                h = wordH[i][j];
                v = wordV[i][j];
                if (sameColor != colors[v][h]) {
                    sameColor = 0;
                }
            }
            if (sameColor == 1 || sameColor == 2) {
                int offset = BOX_SIZE / 2 - 1;
                if (sameColor == 1) {
                    bg.setColor(playerGreen);
                    score0++;
                } else {
                    bg.setColor(playerRed);
                    score1++;
                }
                for (int j = 0; j < wordLen[i] - 1; j++) {
                    h = wordH[i][j];
                    v = wordV[i][j];
                    if (v == wordV[i][j + 1]) {
                        bg.fillRect(BOX_SIZE * h + offset, BOX_SIZE * v + offset, BOX_SIZE + 3, 3);
                    } else {
                        bg.fillRect(BOX_SIZE * h + offset, BOX_SIZE * v + offset, 3, BOX_SIZE + 3);
                    }
                }
            }
        }

        // Solution letters
        int yAdjust = (BOX_SIZE + solutionMetrics.getAscent()) / 2;
        bg.setFont(solutionFont);
        for (int i = 0; i < NUM_VERTICAL; i++) {
            int yPos = i * BOX_SIZE + yAdjust;
            for (int j = 0; j < NUM_HORIZONTAL; j++) {
                if (solution[i][j] != '_') {
                    int xPos = j * BOX_SIZE + (BOX_SIZE - solutionMetrics.charWidth(solution[i][j])) / 2;
                    // Different cases
                    if (colors[i][j] < 0) // Show solution
                    {
                        bg.setColor(Color.BLACK);
                        bg.drawChars(solution[i], j, 1, xPos, yPos);
                    } else if (colors[i][j] > 0) {
                        if (colors[i][j] == 1) // Green
                        {
                            score0++;
                            bg.drawImage(greenBlob, BOX_SIZE * j + blobOffset, BOX_SIZE * i + blobOffset, this);
                        } else if (colors[i][j] == 2) // Red
                        {
                            score1++;
                            bg.drawImage(redBlob, BOX_SIZE * j + blobOffset, BOX_SIZE * i + blobOffset, this);
                        } else {
                            bg.drawImage(grayBlob, BOX_SIZE * j + blobOffset, BOX_SIZE * i + blobOffset, this);
                        }
                        bg.setColor(Color.WHITE);
                        bg.drawChars(solution[i], j, 1, xPos, yPos);
                    }
                }
            }
        }

        // Clues
        for (int i = 0; i < numKeys; i++) {
            if (visibleClues[i]) {
                bg.setColor(Color.BLACK);
            } else {
                bg.setColor(Color.LIGHT_GRAY);
            }
            int useFont = clueFontSize[i];
            int numLines = keyTexts[i].length;
            bg.setFont(clueFonts[useFont]);
            int fontDist = clueMetrics[useFont].getHeight();
            int minMargin = clueMetrics[useFont].getDescent();
            while (fontDist > 0 && (numLines - 1) * fontDist + clueMetrics[useFont].getAscent() + 2 * minMargin > clueH[i]) {
                fontDist--;
            }
            int y = clueY[i] + (clueH[i] - (numLines - 1) * fontDist + clueMetrics[useFont].getAscent()) / 2;
            for (int j = 0; j < numLines; j++) {
                bg.drawString(keyTexts[i][j], clueX[i]
                        + (clueW[i] - clueMetrics[useFont].stringWidth(keyTexts[i][j])) / 2, y);
                y += fontDist;
            }
        }
    }

    public void exportToFile(String outF) {

        savePng(backBuffer, outF);
    }

    public static void savePng(BufferedImage bI, String nam) {
        try {
            Iterator iterator = ImageIO.getImageWritersBySuffix("png");
            if (!iterator.hasNext()) {
                throw new IllegalStateException("no writers found");
            }
            ImageWriter iw = (ImageWriter) iterator.next();
            FileOutputStream os = new FileOutputStream(nam);
            ImageOutputStream ios = ImageIO.createImageOutputStream(os);
            iw.setOutput(ios);
            ImageWriteParam iwp = iw.getDefaultWriteParam();
            iw.write(bI);
            System.out.println("Saved " + nam);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearBoard() {
        for (int i = 0; i < visibleClues.length; i++) {
            visibleClues[i] = false;
        }
        for (int i = 0; i < NUM_VERTICAL; i++) {
            for (int j = 0; j < NUM_HORIZONTAL; j++) {
                colors[i][j] = 0;
            }
        }
        deblur = 0;
    }

    public void showEverything() {
        for (int i = 0; i < visibleClues.length; i++) {
            visibleClues[i] = true;
        }
        for (int i = 0; i < NUM_VERTICAL; i++) {
            for (int j = 0; j < NUM_HORIZONTAL; j++) {
                colors[i][j] = -1;
            }
        }
        logLines = null;
        deblur = 2;
    }

    public void stepLogBack() {
        if (logLines != null) {
            boolean lineFound = false;
            while (currentLogLine > 0 && !lineFound) {
                currentLogLine--;
                if (logLines[currentLogLine].indexOf(STRING1) > 0
                        || logLines[currentLogLine].indexOf(STRING2) > 0
                        || logLines[currentLogLine].indexOf(STRING3) > 0
                        || logLines[currentLogLine].indexOf(STRING4) > 0) {
                    extractMatchData(logLines[currentLogLine]);
                    lineFound = true;
                }
            }
        }
    }

    public void stepLogForward() {
        if (logLines != null) {
            boolean lineFound = false;
            while (currentLogLine < logLines.length - 1 && !lineFound) {
                currentLogLine++;
                if (logLines[currentLogLine].indexOf(STRING1) > 0
                        || logLines[currentLogLine].indexOf(STRING2) > 0
                        || logLines[currentLogLine].indexOf(STRING3) > 0
                        || logLines[currentLogLine].indexOf(STRING4) > 0) {
                    extractMatchData(logLines[currentLogLine]);
                    lineFound = true;
                }
            }
        }
    }

    public void beginLog() {
        if (logLines != null) {
            currentLogLine = 0;
            clearBoard();
        }
    }

    public void endLog() {
        if (logLines != null) {
            currentLogLine = logLines.length;
            boolean lineFound = false;
            while (currentLogLine > 0 && !lineFound) {
                currentLogLine--;
                if (logLines[currentLogLine].indexOf(STRING1) > 0
                        || logLines[currentLogLine].indexOf(STRING2) > 0
                        || logLines[currentLogLine].indexOf(STRING3) > 0
                        || logLines[currentLogLine].indexOf(STRING4) > 0) {
                    extractMatchData(logLines[currentLogLine]);
                    lineFound = true;
                }
            }

        }
    }

    // Get the actual match state, with filled-in letters,
    // revealed clues and picture deblur level
    public void extractMatchData(String s) {
        clearBoard();
        int k = s.indexOf("colors");
        while (s.charAt(k) < '0' || s.charAt(k) > '9') {
            k++;
        }
        for (int i = 0; i < NUM_VERTICAL; i++) {
            for (int j = 0; j < NUM_HORIZONTAL; j++) {
                colors[i][j] = s.charAt(k++) - '0';
            }
        }
        for (int i = 0; i < visibleClues.length; i++) {
            visibleClues[i] = false;
        }
        k = s.indexOf("\"revealed\"");
        if (k > 0) {
            while (s.charAt(k) != '[') {
                k++;
            }
            k++;
            int l = k;
            while (s.charAt(l) != ']') {
                l++;
            }
            String rev = s.substring(k, l);
            StringTokenizer st = new StringTokenizer(rev, ",");
            while (st.hasMoreTokens()) {
                String el = st.nextToken();
                try {
                    int ix = Integer.parseInt(el);
                    visibleClues[ix] = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        k = s.indexOf("\"deblur\"");
        if (k > 0) {
            while (s.charAt(k) < '0' || s.charAt(k) > '2') {
                k++;
            }
            deblur = s.charAt(k) - '0';
        } else {
            deblur = 0;
        }
    }

}
