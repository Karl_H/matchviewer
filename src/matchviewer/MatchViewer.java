/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matchviewer;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FileDialog;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author MacMini
 */
public class MatchViewer extends JFrame implements ActionListener {

    CrosswordPanel myPanel;
    TextField crosswordField;
    TextField matchField;
    static Button crosswordButton;
    static Button matchButton;
    static Button startButton, backButton, forwardButton, endButton;
    static Button snapshotButton;

    TextField score0;
    TextField score1;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                MatchViewer mv = new MatchViewer();
                mv.setVisible(true);
            }
        });
    }

    public MatchViewer() {
        setTitle("Match Viewer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myPanel = new CrosswordPanel();
        myPanel.initSize();

        JPanel outerPanel = new JPanel(new BorderLayout());
        outerPanel.add(myPanel, BorderLayout.WEST);
        JPanel eastPanel = new JPanel();
        JPanel packPanel = new JPanel(new BorderLayout());
        eastPanel.setLayout(new BoxLayout(eastPanel, BoxLayout.Y_AXIS));
        eastPanel.add(new Label("Crossword file"));
        crosswordField = new TextField("");
        crosswordField.setEditable(false);
        eastPanel.add(crosswordField);
        crosswordButton = new Button("Load zip");
        crosswordButton.addActionListener(this);

        eastPanel.add(crosswordButton);

        eastPanel.add(new Label("Match log file"));
        matchField = new TextField("", 30);
        matchField.setEditable(false);
        eastPanel.add(matchField);
        matchButton = new Button("Load log");
        matchButton.addActionListener(this);
        eastPanel.add(matchButton);

        eastPanel.add(new Label("Replay match"));
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
        startButton = new Button("<<");
        startButton.addActionListener(this);
        backButton = new Button("<");
        backButton.addActionListener(this);
        forwardButton = new Button(">");
        forwardButton.addActionListener(this);
        endButton = new Button(">>");
        endButton.addActionListener(this);
        buttonsPanel.add(startButton);
        buttonsPanel.add(backButton);
        buttonsPanel.add(forwardButton);
        buttonsPanel.add(endButton);
        eastPanel.add(buttonsPanel);
        disablePlayButtons();

        JPanel scorePanel = new JPanel();
        scorePanel.add(new Label("Green score:"));
        score0 = new TextField(3);
        score0.setEditable(false);
        scorePanel.add(score0);
        scorePanel.add(new Label("Red score:"));
        score1 = new TextField(3);
        score1.setEditable(false);
        scorePanel.add(score1);
        eastPanel.add(scorePanel);

        snapshotButton = new Button("Snapshot");
        snapshotButton.addActionListener(this);
        eastPanel.add(snapshotButton);

        packPanel.add(eastPanel, BorderLayout.NORTH);
        outerPanel.add(packPanel, BorderLayout.EAST);

        getContentPane().add(outerPanel);
        pack();
    }

    public static void disablePlayButtons() {
        startButton.setEnabled(false);
        backButton.setEnabled(false);
        forwardButton.setEnabled(false);
        endButton.setEnabled(false);
    }

    public static void enablePlayButtons() {
        startButton.setEnabled(true);
        backButton.setEnabled(true);
        forwardButton.setEnabled(true);
        endButton.setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        if (cmd.equalsIgnoreCase("Load zip")) {
            showOpenCrosswordDialog();
        } else if (cmd.equalsIgnoreCase("Load log")) {
            showOpenLogDialog();
        } else if (cmd.equalsIgnoreCase("<<")) {
            // Step back to beginning
            myPanel.beginLog();
            myPanel.drawCrossword();
            myPanel.repaint();
            updateScores();
        } else if (cmd.equalsIgnoreCase("<")) {
            // Take a step back to the previous move
            myPanel.stepLogBack();
            myPanel.drawCrossword();
            myPanel.repaint();
            updateScores();
        } else if (cmd.equalsIgnoreCase(">")) {
            // Take a step forward to the next move
            myPanel.stepLogForward();
            myPanel.drawCrossword();
            myPanel.repaint();
            updateScores();
        } else if (cmd.equalsIgnoreCase(">>")) {
            // Jump to the end
            myPanel.endLog();
            myPanel.drawCrossword();
            myPanel.repaint();
            updateScores();
        } else if (cmd.equalsIgnoreCase("Snapshot")) {
            // Export match state as image
            showSnapshotFileDialog();
        }
    }

    // For selecting crossword zip file
    public void showOpenCrosswordDialog() {
        FileDialog fileDialog = new FileDialog(
                this, "Open crossword zip file", FileDialog.LOAD);
        fileDialog.setFilenameFilter(new ZipFilter());
        fileDialog.setVisible(true);
        String fileName = fileDialog.getFile();

        if (fileName == null) {
            System.out.println("No file was selected");
        } else {
            crosswordField.setText(fileName);
            myPanel.loadCrossword(fileDialog.getDirectory() + fileName);
            matchField.setText("");
            disablePlayButtons();
            myPanel.drawCrossword();
            myPanel.repaint();
            clearScores();
        }
    }

    // For selecting log text file
    public void showOpenLogDialog() {
        FileDialog fileDialog = new FileDialog(
                this, "Open log txt file", FileDialog.LOAD);
        fileDialog.setFilenameFilter(new LogFilter());
        fileDialog.setVisible(true);
        String fileName = fileDialog.getFile();

        if (fileName == null) {
            System.out.println("No file was selected");
        } else {
            matchField.setText(fileName);
            myPanel.loadLog(fileDialog.getDirectory() + fileName);
            enablePlayButtons();
            myPanel.drawCrossword();
            myPanel.repaint();
            clearScores();
        }
    }

    public void showSnapshotFileDialog() {
        FileDialog fileDialog = new FileDialog(
                this, "Exportera", FileDialog.SAVE);
        fileDialog.setFile("snapshot.png");
        fileDialog.setVisible(true);
        String fileName = fileDialog.getFile();
        if (fileName == null) {
            System.out.println("No file was selected");
        } else {
            myPanel.exportToFile(fileDialog.getDirectory() + fileName);
        }
    }

    public void clearScores() {
        score0.setText("");
        score1.setText("");
    }

    public void updateScores() {
        score0.setText("" + myPanel.score0);
        score1.setText("" + myPanel.score1);
    }

}
